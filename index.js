/*
	npm init -y  /  npm init
	npm install express
	npm install mongoose
*/

// ----------------------------------
/*
	This code will help us access contents of express module/package
		A "module" is a software component or part of a program that contains one or more routines
	It also allows us to access methods and functions to easily create a server
	We store our express module to a variable so we could easily access its keywords, functions, and methods
*/

const express = require("express");
const mongoose = require("mongoose");

const app = express();

const port = 3001;

// Setup for allowing the server to handle data from requests
// Allows your app to read json data
app.use(express.json());

// Allows your app to read data from forms
app.use(express.urlencoded({extended:true}));
//Reference: https://dev.to/griffitp12/express-s-json-and-urlencoded-explained-1m7o

// Insert codes here...


mongoose.connect("mongodb+srv://admin:admin@b218-to-do.arttd8q.mongodb.net/toDo?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// [SECTION] Creating Schema
/*
	// Schemas determine the structure of the documents to be written in the database
	// Schemas act as blueprints to our data
	// Use the Schema() constructor of the Mongoose module to create a new schema object
	// The "new" keyword creates a new Schema
	// Syntax:
	
		const schemaName = new mongoose.Schema({<keyvalue:pair>});
	
	// name & status
	// "required" is used to specify that a field must not be empty.
	// "default" is used if a field value is not supplied.
*/

	const taskSchema = new mongoose.Schema({
		name: {
			type: String,
			required: [true, "Task name is required"]
		},
		status: {
			type: String,
			default: "pending"
		}
	});

/*
	//[SECTION] Models
	// The variable/object "Task"can now be used to run commands for interacting with our database
	// Models must be in singular form and capitalized
	// The first parameter of the Mongoose model method indicates the collection in where to store the data
	// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
*/
		//modelName           //collectionName //schemaName
	const Task = mongoose.model("Task", taskSchema);

// ----------------------------------------------------


// [SECTION] POST / insert
// First Parameter is for request, second parameter is for response
app.post("/tasks", (request,response)=>{

	console.log(request.body.name);

	// Goal: to check if there are duplicate tasks
	// "findOne" is a Mongoose method that acts similar to "find" of MongoDb
	// findOne() returns the first document that matches the search criteria
	// If there are no matches, the value of result is null
	// "err" is a shorthand naming convention for errors
	Task.findOne({name: request.body.name}, (err,result)=>{
		if(result != null && result.name == request.body.name){

			// Returns a message to the client/Postman
			return response.send("Duplicate task found")
		}
		// If no document was found
		else{

			// This is where we receive our field values
			let newTask = new Task ({
				name: request.body.name
			})

			// The "save" method will store the information to the database
			newTask.save((saveError, savedTask) => {

				if(saveError){

					// Will print any errors found in the console
					// saveError is an error object that will contain details about the error
						// Errors normally come as an object data type
					return console.error(saveError);
				}

				// No error/s found
				else{

					// Return a status code of 201 - means success or OK
					// Sends a message upon successful saving
					return response.status(201).send("New task created")
				}

			})
		}
	})

});


// [SECTION] GET Method

app.get("/tasks", (request, response) => {

	Task.find({ name: "King James"}, (err, result) => {
		if(err){
			return console.log(err);
		}
		else {
			return response.status(201).json({
				data: result
			})
		}
	})

});


//ACTIVITY
/*
	1. Create a User schema.
		username - string
		password - string
	2. Create a User model.
	Take a screenshot of your mongoDB collection to show that the users collection is added.
*/

/*
	3. Create a route for creating a user, the endpoint should be "/signup" and the http method to be used is 'post'.
	4. Use findOne and conditional statement to check if there is already an existing username. If ther is no match (else), it should successfully create a new user with password.
	5. If there is no error encountered during the process (include status code 201), the program should send a response "New user registered"
*/

// Enter Activity Code Here

// 1. User schema
const userSchema = new mongoose.Schema({
    username : String,
    password : String
});

// 2. User model
const User = mongoose.model("User", userSchema);

// 3-5.

app.post("/signup", (request, response)=> {

	User.findOne({ username : request.body.username }, (err, result) => {

		if(result != null && result.username == request.body.username){

			return response.send("Duplicate username found!");

		} else {

			// If the username is blank
			if (request.body.username == "" && request.body.password !== ""){

				return response.send("BAWAL PO ANG BLANGKONG USERNAME.");
			
			// If the password is blank
			} else if (request.body.username !== "" && request.body.password == ""){

				return response.send("BAWAL PO ANG BLANGKONG PASSWORD.");
			
			// If username and password are not blank
			} else if(request.body.username !== "" && request.body.password !== ""){

                let newUser = new User({
                    username : request.body.username,
                    password : request.body.password
                });

                newUser.save((saveError, savedTask) => {

                    if(saveError){

                        return console.error(saveError);

                    } else {

                        return response.status(201).send("New user registered");

                    }

                })

            // If the username or password are both blank
            } else {

                return response.send("PAKIBIGAY PO NG INYONG USERNAME AND PASSWORD.");
            }			
		}
	})
});

app.listen(port, ()=> console.log(`Server is running at port ${port}`));
// app.listen(port, ()=> console.log("Server is running at port" + port);

